# Zabbix Notifier
Python utility to connect to Zabbix API and notify with popup messages on event triggers

To use this program, you need to define a "credentials" file in the same directory as the script. It should have five lines, in this format (and must end with a newline character):

<pre>
url of Zabbix server
session username (for accessing the server)
session password
API login username (for actually logging in)
API login password
</pre>

For obvious reasons, this credentials file should not be group or world readable.

When the script starts, it will look for a "config" file which contains information about which events you want to receive messages about. If it fails to find the file, it will ask you to choose which ones you'd like to see.

After initial setup is completed, it begins the main loop, polling the API every five minutes. It gets a list of event triggers, and uses keyword searches to decide which ones to show the users. If there are any events, it builds a report and displays it in the foreground in a Tkinter window. After clicking the "Close" button, the program resumes the loop.

## Python 3
Works out of the box! Uses a try-catch on the import to make sure the correct Tkinter library is loaded.

## Dependencies
tkinter

psutil

pyzabbix

os

sys
