#!/usr/bin/env python
# Author: C. Pitts
# Date: March 15, 2016

# Imports
from pyzabbix import ZabbixAPI
import time
import os
import pwd
import psutil
import sys
# Version-agnostic import of Tkinter (Python 3/2.7.6)
try:
    from Tkinter import Tk, Text, Button, Label, Checkbutton, IntVar
    from Tkinter import INSERT, DISABLED, W
except ImportError:
    from tkinter import Tk, Text, Button, Label, Checkbutton, IntVar
    from tkinter import INSERT, DISABLED, W


# Globals
# Delay (in seconds) between checking
DELAY = 300
# Track already-handled events
ids = set()
CONFIGFILE = "config"
# Checkboxes
checkboxes = []
# Variables
variables = dict()
# Settings
settings = []
# Keywords  format option:keywords:title
keywords = {"Down Host": "unreachable:Down hosts",
            "Low Diskspace": "less than 10%:Disk space less than 10%",
            "High Traffic": "Disk IO is high on:High Disk I/O",
            "Restart": "restarted:Just restarted",
            "Missing Files": "missing on:Files missing",
            "File Changed": "changed on:Files changed",
            "Processes": "processes:Too many processes"}
# Titles
titles = dict()


def loggedIn(username):
    users = psutil.users()
    for user in users:
        if username == user.name:
            return True
    return False


def closeWindow(dialog):
    dialog.destroy()


def saveSettings(window):
    with open(CONFIGFILE, 'w') as settingsFile:
        options = keywords.keys()
        for opt in options:
            settingsFile.write("{}:{}\n".format(opt, str(variables[opt].get())))
    window.destroy()


def showHosts(message):
    # Popup window
    dialog = Tk()
    # Set message
    text = Text(dialog)
    text.insert(INSERT, message)
    text.config(state=DISABLED)
    text.pack()
    # Button
    button = Button(dialog, text="Close", command=dialog.destroy)
    button.pack()
    # Enter to exit
    # Set title
    dialog.wm_title("Zabbix Notifier")
    # Raise to top
    dialog.lift()
    # Start loop
    dialog.mainloop()


def setConfig():
    # Dialog
    settingsWindow = Tk()

    # Add label
    Label(settingsWindow, text="Alert on:").grid(row=0, sticky=W)

    # Add checkboxes
    index = 1
    options = keywords.keys()
    options.sort()
    for keyword in options:
        var = IntVar()
        variables[keyword] = var
        box = Checkbutton(settingsWindow, text=keyword, variable=var)
        box.grid(row=index, sticky=W)
        index = index + 1

    # Button
    Button(settingsWindow,
           text="Exit (no save)",
           command=settingsWindow.destroy).grid(row=index + 1, column=0, sticky=W)
    Button(settingsWindow,
           text="Save",
           command=lambda: saveSettings(settingsWindow)).grid(row=(index + 1), column=1, sticky=W)

    # Set title
    settingsWindow.wm_title("Zabbix Notifier Configuration")
    # Raise to top
    settingsWindow.lift()
    # Start loop
    settingsWindow.mainloop()
# end setConfig


def loadConfig(settings):
    if not os.path.isfile(CONFIGFILE):
        print("Missing config file!")
        exit()
    with open(CONFIGFILE) as configFile:
        for line in configFile:
            parts = line.split(':')
            option = str(parts[0])
            value = str(parts[1][:-1])
            if value is "1":
                pair = keywords[option].split(':')
                settings.append(pair[0])
                titles[pair[0]] = pair[1]


if __name__ == "__main__":
    # Check for config file
    if not os.path.isfile(CONFIGFILE):
        setConfig()

    loadConfig(settings)

    # Load credentials
    zabbixURL = ""
    sessionUser = ""
    sessionPassword = ""
    apiUser = ""
    apiPassword = ""

    with open("credentials", 'r') as credFile:
        zabbixURL = credFile.readline()[:-1]
        sessionUser = credFile.readline()[:-1]
        sessionPassword = credFile.readline()[:-1]
        apiUser = credFile.readline()[:-1]
        apiPassword = credFile.readline()[:-1]

    # Connect to Zabbix API
    zabbix = ZabbixAPI(zabbixURL)
    # Authorize as user
    zabbix.session.auth = (sessionUser, sessionPassword)
    # Login
    zabbix.login(apiUser, apiPassword)

    # Try
    try:
        while loggedIn(pwd.getpwuid(os.getuid()).pw_name):
            # Pull list of issues
            issues = zabbix.trigger.get(only_true=1,
                                        skipDependent=1,
                                        monitored=1, active=1,
                                        output="extend",
                                        expandDescription=1,
                                        expandData="host")
            message = ""
            show = False
            # Iterate over events
            for event in settings:
                if message is not "":
                    message = message + '\n'

                message = message + titles[event]
                # Iterate over issues
                for item in issues:
                    if event in item["description"]:
                        # Check for events already handled
                        if item["triggerid"] not in ids:
                            show = True
                            # Build message
                            host_message = item["hostname"]
                            # Manage newlines
                            if len(message) > 0:
                                host_message = '\n' + host_message

                            # Append to message
                            message = message + host_message
                            # Add to already-notified list
                            ids.add(item["triggerid"])

                message = message + '\n'

            if show is True:
                showHosts(message)

            time.sleep(DELAY)

    except KeyError:
        sys.exit(0)
